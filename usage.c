#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "hidparse.h"

struct usage_page *first_usage_page = 0;
struct usage_page *last_usage_page = 0;

static void set_current_usage_page(char *index_string, char *id)
{
	int index;
	struct usage_page *u;

	sscanf(index_string, "%i", &index);

	u = malloc(sizeof(struct usage_page));

	u->index = index;
	u->id = strdup(id);
	u->first_usage = 0;
	u->last_usage = 0;
	u->next = 0;

	if (!first_usage_page)
		first_usage_page = u;
	else
		last_usage_page->next = u;

	last_usage_page = u;
}

static void add_usage_to_page(struct usage_page *p, char *index_string, char *id)
{
	int index;
	struct usage *u;

	sscanf(index_string, "%i", &index);

	u = malloc(sizeof(struct usage));
	u->index = index;
	u->id = strdup(id);
	u->next = 0;

	if (!p->first_usage)
		p->first_usage = u;
	else
		p->last_usage->next = u;

	p->last_usage = u;
}

struct usage_page *find_usage_page_by_id(char *id)
{
	struct usage_page *p;

	for (p=first_usage_page;p;p=p->next)
		if (strcmp(id, p->id) == 0)
			return p;
	return 0;
}

struct usage_page *find_usage_page_by_index(int index)
{
	struct usage_page *p;

	for (p=first_usage_page;p;p=p->next)
		if (p->index == index)
			return p;
	return 0;
}

struct usage *find_usage_by_id(struct usage_page *p, char *id)
{
	struct usage *u;

	for (u=p->first_usage;u;u=u->next)
		if (strcmp(id, u->id) == 0)
			return u;
	return 0;
}

struct usage *find_usage_by_index(struct usage_page *p, int index)
{
	struct usage *u;

	for (u=p->first_usage;u;u=u->next)
		if (u->index == index)
			return u;
	return 0;
}

void dump_usages(struct usage_page *p)
{
	struct usage *u;

	for (u=p->first_usage;u;u=u->next) {
		printf ("\t%03d: %s\n", u->index, u->id);
	}
}

void dump_usage_pages()
{
	struct usage_page *p;

	for (p=first_usage_page;p;p=p->next) {
		printf ("USAGE PAGE %03d: %s\n", p->index, p->id);
		dump_usages(p);
	}
}

void load_usage_db(const char *fname)
{
	FILE *fp;
	char *line;

	fp = fopen(fname, "r");
	if (!fp) {
		fprintf(stderr, "can't open usages database '%s'\n", fname);
		exit(1);
	}

	line = malloc(1024);
	if (!line) {
		fprintf(stderr, "can't allocate line buffer\n", line);
		exit(1);
	}

	while (!feof(fp)) {
		if (fgets(line, 1023, fp)) {
			char *index, *id, *p;

			if (line[0] == '#' || line[0] == '\n' || line[0] == '\r')
				continue;

			if (line[0] != '\t') {
				index = strtok_r(line, "\t", &p);
				id = strtok_r(NULL, "\t\n\r", &p);

				set_current_usage_page(index, id);
			} else if (line[1] != '\t') {
				index = strtok_r(line+1, "\t", &p);
				id = strtok_r(NULL, "\t\n\r", &p);

				add_usage_to_page(last_usage_page, index, id);
			} else {
				fprintf (stderr, "ERROR: malformed line in usage db.\n");
				exit(1);
			}
		}
	}

	free(line);
}

