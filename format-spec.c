#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>

#include "hidparse.h"

struct parser_context {
	struct hid_item *head;
	struct hid_item *tail;
	struct usage_page *usage_page;
};

struct item_dict {
	int tag;
	char *string;
	int (*value_parser)(struct parser_context *, const char *);
};

struct collection {
	int index;
	const char *id;
} collection[] = {
	{ 0, "Physical" },
	{ 1, "Application" },
	{ 2, "Logical" },
	{ 3, "Report" },
	{ 4, "Named Array" },
	{ 5, "Usage Switch" },
	{ 6, "Usage Modifier" },
	{ -1, NULL },
};

static int io_value_parser(struct parser_context *, const char *);
static int usage_page_parser(struct parser_context *, const char *);
static int usage_parser(struct parser_context *, const char *);
static int collection_parser(struct parser_context *, const char *);

struct item_dict main_items[] = {
	{ Input, "Input", io_value_parser },
	{ Output, "Output", io_value_parser },
	{ Collection, "Collection", collection_parser },
	{ Feature, "Feature" },
	{ EndCollection, "End Collection" },
	{ -1, NULL },
};

struct item_dict global_items[] = {
	{ UsagePage, "Usage Page", usage_page_parser },
	{ LogicalMinimum, "Logical Minimum" },
	{ LogicalMaximum, "Logical Maximum" },
	{ PhysicalMinimum, "Physical Minimum" },
	{ PhysicalMaximum, "Physical Maximum" },
	{ UnitExponent, "Unit Exponent" },
	{ Unit, "Unit" },
	{ ReportSize, "Report Size" },
	{ ReportID, "Report ID" },
	{ ReportCount, "Report Count" },
	{ Push, "Push" },
	{ Pop, "Pop" },
	{ -1, NULL },
};

struct item_dict local_items[] = {
	{ Usage, "Usage", usage_parser },
	{ UsageMinimum, "Usage Minimum" },
	{ UsageMaximum, "Usage Maximum" },
	{ DesignatorIndex, "Designator Index" },
	{ DesignatorMinimum, "Designator Minimum" },
	{ DesignatorMaximum, "Designator Maximum" },
	{ StringIndex, "String Index" },
	{ StringMinimum, "String Minimum" },
	{ StringMaximum, "String Maximum" },
	{ Delimiter, "Delimiter" },
	{ -1, NULL },
};


static struct collection *find_collection_by_index(int index)
{
	struct collection *c = &collection[0];

	while (c->id) {
		if (c->index == index)
			return c;
		c++;
	}

	return 0;
}



/* Every parser should consume all trailing whitespace */

/*
 * scan for [a-zA-Z][ a-zA-Z0-9]*
 */
static int parse_id(const char *str)
{
	int count = 1;

	if (!isalpha(str[0]))
		return 0;

	while (isalpha(str[count]) || isdigit(str[count]) || str[count] == ' ')
		count++;

	return count;
}

/* -?[0-9]+ */
static int parse_number(const char *str)
{
	int count = 0;

	if (*str == '-')
		count++;

	while (isdigit(str[count]))
		count++;

	return count;
}

static int skip_whitespace(const char *str)
{
	int i = 0;
	while (isspace(str[i]))
		i++;
	return i;
}

static void remove_trailing_whitespace(char *s)
{
	char *p;

	while (*s)
		s++;

	while (*--s == ' ')
		*s = 0;
}

static void lower_case(char *s)
{
	while (*s) {
		*s = tolower(*s);
		s++;
	}
}

static int copy_id_lowercase(char *dst, const char *src, int max)
{
	int n = parse_id(src);

	if (n <= 0)
		return 0;

	n = max < n ? max : n;

	strncpy(dst, src, n);
	dst[n] = 0;
	remove_trailing_whitespace(dst);
	lower_case(dst);

	return n;
}


static int copy_id(char *dst, const char *src, int max)
{
	int n = parse_id(src);

	if (n <= 0)
		return 0;

	n = max < n ? max : n;

	strncpy(dst, src, n);
	dst[n] = 0;
	remove_trailing_whitespace(dst);

	return n;
}

static int match_item(struct item_dict *dict, const char *str)
{
	int n = parse_id(str);
	char sbuf1[64];
	char sbuf2[64];
	int i;

	if (!n)
		return -1;

	copy_id_lowercase(sbuf1, str, n);

	for (i=0;dict[i].string;i++)  {
		strncpy(sbuf2, dict[i].string, 64);
		lower_case(sbuf2);

		if (strcmp(sbuf1, sbuf2) == 0)
			return dict[i].tag;
	}

	return -1;
}

static int usage_page_parser(struct parser_context *ctx, const char *str)
{
	struct usage_page *p;
	char buf[64];

	copy_id(buf, str, 63);
	p = find_usage_page_by_id(buf);

	if (!p) {
		p->index = 0;
		return 0;
	}

	ctx->tail->value = p->index;
	ctx->usage_page = p;

	return 0;
}

static int usage_parser(struct parser_context *ctx, const char *str)
{
	struct usage *u;
	char buf[64];

	if (!ctx->usage_page)
		return 0;

	copy_id(buf, str, 63);
	u = find_usage_by_id(ctx->usage_page, buf);
	if (u)
		ctx->tail->value = u->index;

	return 0;
}

static int io_value_parser(struct parser_context *ctx, const char *str)
{
	char sbuf[64];
	int n = parse_id(str);
	int val = ctx->tail->value;

	copy_id_lowercase(sbuf, str, n);

	if        (strcmp(sbuf, "data") == 0) {
		val &= ~0x01;
	} else if (strcmp(sbuf, "constant") == 0) {
		val |= 0x01;
	} else if (strcmp(sbuf, "array") == 0) {
		val &= ~0x02;
	} else if (strcmp(sbuf, "variable") == 0) {
		val |= 0x02;
	} else if (strcmp(sbuf, "absolute") == 0) {
		val &= ~0x04;
	} else if (strcmp(sbuf, "relative") == 0) {
		val |= 0x04;
	} else if (strcmp(sbuf, "no wrap") == 0) {
		val &= ~0x08;
	} else if (strcmp(sbuf, "wrap") == 0) {
		val |- 0x08;
	} else if (strcmp(sbuf, "linear") == 0) {
		val &= ~0x10;
	} else if (strcmp(sbuf, "non linear") == 0) {
		val |= 0x10;
	} else if (strcmp(sbuf, "preferred state") == 0) {
		val &= ~0x20;
	} else if (strcmp(sbuf, "no preferred") == 0) {
		val |= 0x20;
	} else if (strcmp(sbuf, "no null position") == 0) {
		val &= 0x40;
	} else if (strcmp(sbuf, "null state") == 0) {
		val |= 0x40;
	} else if (strcmp(sbuf, "bit field") == 0) {
		val &= ~0x80;
	} else if (strcmp(sbuf, "buffered bytes") == 0) {
		val |= 0x80;
	} else {
		return -1;
	}

	ctx->tail->value = val;
}

static int collection_parser(struct parser_context *ctx, const char *str)
{
	char sbuf[64];
	int n = parse_id(str);
	int val;

	copy_id_lowercase(sbuf, str, n);

	if        (strcmp(sbuf, "physical") == 0) {
		val = 0;
	} else if (strcmp(sbuf, "application") == 0) {
		val = 1;
	} else if (strcmp(sbuf, "logical") == 0) {
		val = 2;
	} else if (strcmp(sbuf, "report") == 0) {
		val = 3;
	} else if (strcmp(sbuf, "named array") == 0) {
		val = 4;
	} else if (strcmp(sbuf, "usage switch") == 0) {
		val = 5;
	} else if (strcmp(sbuf, "usage modifier") == 0) {
		val = 6;
	} else {
		/* last attempt: try to read an integer */
		sscanf(sbuf, "%i", &val);
	}

	ctx->tail->value = val;

	return 0;
}

/*
 * Descriptor Syntax:
 *
 * report_descriptor: item_list
 * item_list: item ( COMMA item_list )?
 * item: id ( LEFT_PAREN value_list RIGHT_PAREN )?
 * value_list: value ( COMMA value_list )?
 * value: id | number
 */
static int parse_item(struct parser_context *ctx, const char *str);
static int parse_value_list(struct parser_context *ctx, const char *str);
static int parse_value(struct parser_context *ctx, const char *str);

static int parse_item_list(struct parser_context *ctx, const char *str)
{
	int total_count = 0;

	while (str[total_count]) {
		int count;

		count = parse_item(ctx, str + total_count);
		if (!count) {
			break;
		}
		total_count += count;

		/* Parse a comma */
		if (str[total_count] == ',') {
			total_count++;
			total_count += skip_whitespace(str + total_count);
		} else {
			break;
		}
	}

	return total_count;
}

static int parse_item(struct parser_context *ctx, const char *str)
{
	int total_count;
	int tag;
	int type;
	struct hid_item *item;

	total_count = parse_id(str);
	if (!total_count)
		return 0;

	if      ((tag = match_item(&main_items[0], str)) >= 0)
		type = MAIN_ITEM;
	else if ((tag = match_item(&global_items[0], str)) >= 0)
		type = GLOBAL_ITEM;
	else if ((tag = match_item(&local_items[0], str)) >= 0)
		type = LOCAL_ITEM;
	else
		return 0;

	item = item_new(type, tag);

	if (ctx->head) {
		ctx->tail->next = item;
	} else {
		ctx->head = item;
	}

	ctx->tail = item;

	if (str[total_count] == '(') {
		int count;

		total_count++;
		count = parse_value_list(ctx, str + total_count);
		if (!count)
			return 0;
		total_count += count;

		if (str[total_count] != ')')
			return 0;

		total_count++;
		total_count += skip_whitespace(str + total_count);
	}

	return total_count;
}

static int parse_value_list(struct parser_context *ctx, const char *str)
{
	int total_count = 0;

	total_count = parse_value(ctx, str);
	if (!total_count) {
		fprintf(stderr, "ERROR!\n");
		return 0;
	}

	while (str[total_count] == ',') {
		int count;

 		total_count++;
		total_count += skip_whitespace(str + total_count);
		count = parse_value(ctx, str + total_count);

		if (!count) {
			fprintf(stderr, "ERROR!\n");
			return 0;
		}

		total_count += count;
	}

	return total_count;
}

static struct item_dict *item_dict_by_type(int type)
{
	switch (type) {
		case MAIN_ITEM:
			return &main_items[0];
		case GLOBAL_ITEM:
			return &global_items[0];
		case LOCAL_ITEM:
			return &local_items[0];
		default:
			return 0;
	}
}

static struct item_dict *item_dict_by_type_and_tag(int type, int tag)
{
	struct item_dict *i;
	struct item_dict *dict = item_dict_by_type(type);

	for (i=dict;i->string;i++) {
		if (i->tag == tag)
			return i;
	}

	return 0;
}

static int parse_value(struct parser_context *ctx, const char *str)
{
	int count;
	struct hid_item *item = ctx->tail;
	long val;

	item->arguments++;

	count = parse_id(str);
	if (count) {
		struct item_dict *d = item_dict_by_type_and_tag(item->type, item->tag);

		if (d->value_parser)
			d->value_parser(ctx, str);

		return count;
	}

	count = parse_number(str);
	if (count)
		sscanf(str, "%i", &ctx->tail->value);

	return count;
}

static struct hid_item *spec_parse_report_descriptor(const char *str, int len)
{
	struct parser_context ctx = {
		.head = NULL,
		.tail = NULL,
		.usage_page = NULL,
	};

	str += skip_whitespace(str);
	if (parse_item_list(&ctx, str))
		return ctx.head;
	else
		return 0;
}

static void spec_print_report_descriptor(struct hid_item *head)
{
	struct hid_item *item;
	struct usage_page *curpage = 0;
	int indent = 0;

	for (item=head;item;item=item->next) {
		struct item_dict *d = item_dict_by_type_and_tag(item->type, item->tag);
		int tabs;

		if (!d) {
			fprintf(stderr, "syntax error: bogus item with type %d, tag %d\n", item->type, item->tag);
			exit(1);
		}

		if ((item->type == MAIN_ITEM) && (item->tag == EndCollection))
			indent--;

		tabs = indent;
		while (tabs--)
			fputc('\t', stdout);

		if ((item->type == MAIN_ITEM) && (item->tag == Collection))
			indent++;

		if (item->arguments) {
			const char *str = 0;
			if ((item->type == GLOBAL_ITEM) && (item->tag == UsagePage)) {
				struct usage_page *p = find_usage_page_by_index(item->value);
				if (p) {
					str = p->id;
					curpage = p;
				}
			} else if (curpage && (item->type == LOCAL_ITEM) && (item->tag == Usage)) {
				struct usage *u = find_usage_by_index(curpage, item->value);

				if (u)
					str = u->id;
			} else if (curpage && (item->type == MAIN_ITEM) && (item->tag == Collection)) {
				struct collection *c = find_collection_by_index(item->value);

				if (c)
					str = c->id;
			}

			if (str)
				printf ("%s (%s)", d->string, str);
			else
				printf ("%s (%d)", d->string, item->value);
		} else {
			printf ("%s", d->string);
		}

		if (item->next)
			fputc(',', stdout);
		fputc('\n', stdout);
	}

}

struct hid_format hid_spec_format = {
	.parse = spec_parse_report_descriptor,
	.print = spec_print_report_descriptor,
};
