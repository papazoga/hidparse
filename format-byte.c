#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>

#include "hidparse.h"

struct parser_context {
	struct hid_item *head;
	struct hid_item *tail;
};

static int signed_item(struct hid_item *item)
{
	if (item->type == GLOBAL_ITEM) {
		switch (item->tag) {
		case LogicalMinimum:
		case LogicalMaximum:
		case PhysicalMinimum:
		case PhysicalMaximum:
			return 1;
		}
	}
	return 0;

}

static int parse_unsigned_value(const unsigned char *buf, int size)
{
	int64_t val;

	switch (size) {
	case 1:
		val = buf[0];
		return val;
		break;
	case 2:
		val = buf[0] | (buf[1] << 8);
		return val;
		break;
	case 3:
		val = buf[0] | (buf[1] << 8) | (buf[2] << 16) | (buf[3] << 24);
		return val;
		break;
	}
}

static int parse_signed_value(const unsigned char *buf, int size)
{
	int64_t val;

	switch (size) {
	case 1:
		val = (char)buf[0];
		break;
	case 2:
		val = (int16_t)(buf[0] | (buf[1] << 8));
		break;
	case 3:
		val = (int32_t)(buf[0] | (buf[1] << 8) | (buf[2] << 16) | (buf[3] << 24));
		break;
	}

	return val;
}

static int parse_item(struct parser_context *ctx, const char *input)
{
	struct hid_item *item;
	const unsigned char *buf = input;
	int type = (*buf >> 2) & 0x3;
	int tag = (*buf >> 4) & 0xf;
	int size = (*buf) & 0x3;
	long long val;

	item = item_new(type, tag);

	if (!ctx->head)
		ctx->head = item;
	else
		ctx->tail->next = item;

	ctx->tail = item;

	if (size == 0)
		return 1;

	item->arguments = 1;

	if (signed_item(item)) {
		item->value = parse_signed_value(buf+1, size);
	} else {
		item->value = parse_unsigned_value(buf+1, size);
	}

	return (size == 3 ? 4 : size) + 1;
}

static struct hid_item *byte_parse_report_descriptor(const char *str, int len)
{
	struct parser_context ctx = { NULL, NULL };
	int index = 0;

	while (index < len) {
		int count = parse_item(&ctx, str + index);
		if (!count) {
			fprintf(stderr, "parse error at %d\n", index);
			return 0;
		}
		index += count;
	}

	return ctx.head;
}

void emit_signed(struct hid_item *item)
{
	unsigned char tts = ((item->tag & 0xf) << 4) | ((item->type & 0x3) << 2);
	int dlen = 0;
	int64_t val;

	if (item->value >= -128 && item->value <= 127) {
		val = (item->value + 0x100) & 0xff;
		dlen = 1;
		tts |= 1;
	} else if (item->value >= -32768 && item->value <= 32767) {
		val = (item->value + 0x10000) & 0xffff;
		dlen = 2;
		tts |= 2;
	} else {
		val = (item->value + 0x100000000) & 0xffffffff;
		dlen = 4;
		tts |= 3;
	}

	fputc(tts, stdout);

	while (dlen--) {
		fputc(val & 0xff, stdout);
		val >>= 8;
	}
}

void emit_unsigned(struct hid_item *item)
{
	unsigned char tts = ((item->tag & 0xf) << 4) | ((item->type & 0x3) << 2);
	int dlen = 0;
	int64_t val = item->value;

	if (item->value <= 255) {
		dlen = 1;
		tts |= 1;
	} else if (item->value <= 65535) {
		dlen = 2;
		tts |= 2;
	} else {
		dlen = 4;
		tts |= 3;
	}

	fputc(tts, stdout);

	while (dlen--) {
		fputc(val & 0xff, stdout);
		val >>= 8;
	}
}

static void byte_print_report_descriptor(struct hid_item *item)
{
	while (item) {
		if (item->arguments == 0) {
			unsigned char tts = ((item->tag & 0xf) << 4) | ((item->type & 0x3) << 2);
			fputc(tts, stdout);
		} else if (signed_item(item)) {
			emit_signed(item);
		} else {
			emit_unsigned(item);
		}

		item = item->next;
	}

	fflush(stdout);
}

struct hid_format hid_byte_format = {
	.parse = byte_parse_report_descriptor,
	.print = byte_print_report_descriptor,
};
