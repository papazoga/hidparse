#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "hidparse.h"

struct hid_item *item_new(int type, int tag)
{
	struct hid_item *item;

	item = malloc(sizeof(struct hid_item));
	item->type = type;
	item->tag = tag;
	item->value = 0;
	item->arguments = 0;
	item->next = NULL;

	return item;
}

struct hid_format *parse_format(const char *s)
{
	if (strcmp(s, "byte") == 0)
		return &hid_byte_format;
	else if (strcmp(s, "spec") == 0)
		return &hid_spec_format;
	else
		return 0;
}

int main(int argc, char **argv)
{
	char *buf;
	struct hid_item *descriptor;
	struct hid_item *item;
	struct usage_page *curpage = 0;
	int len;
	int opt;
	struct hid_format *ifmt = 0, *ofmt = 0;
	char *usage_table_file = "usb_hid_usages";

	while ((opt = getopt(argc, argv, "I:O:u:")) != -1) {
		switch (opt) {
		case 'I':
			ifmt = parse_format(optarg);
			break;
		case 'O':
			ofmt = parse_format(optarg);
			break;
		case 'u':
			usage_table_file = strdup(optarg);
			break;
		}
	}

	if (!ifmt) {
		fprintf(stderr, "invalid input format\n");
		exit(1);
	}

	if (!ofmt) {
		fprintf(stderr, "invalid output format\n");
		exit(1);
	}

	load_usage_db(usage_table_file);

	buf = malloc(4096);
	len = fread(buf, 1, 4096, stdin);

	descriptor = ifmt->parse(buf, len);
	if (descriptor)
		ofmt->print(descriptor);
	else
		fprintf(stderr, "parse error\n");
}
