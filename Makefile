CFLAGS=-g
OBJS=format-spec.o format-byte.o usage.o main.o

hidparse: $(OBJS)
	gcc $(OBJS) -o $@

clean:
	rm -f $(OBJS) hidparse
