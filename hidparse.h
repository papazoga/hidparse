#ifndef _HIDPARSE_H
#define _HIDPARSE_H

struct usage {
	int index;
	char *id;
	struct usage *next;
};

struct usage_page {
	int index;
	char *id;
	struct usage_page *next;
	struct usage *first_usage;
	struct usage *last_usage;
};

struct hid_item {
	int type;
	int tag;
	int value;
	int arguments;
	struct hid_item *next;
};


struct hid_format {
	struct hid_item *(*parse)(const char *, int);
	void (*print)(struct hid_item *);
};

extern struct hid_format hid_spec_format;
extern struct hid_format hid_byte_format;

enum main_item_tags {
	Input = 8,
	Output,
	Collection,
	Feature,
	EndCollection,
	MaxMainItemTags,
};

enum global_item_tags {
	UsagePage,
	LogicalMinimum,
	LogicalMaximum,
	PhysicalMinimum,
	PhysicalMaximum,
	UnitExponent,
	Unit,
	ReportSize,
	ReportID,
	ReportCount,
	Push,
	Pop,
	MaxGlobalItemTags
};

enum local_item_tags {
	Usage,
	UsageMinimum,
	UsageMaximum,
	DesignatorIndex,
	DesignatorMinimum,
	DesignatorMaximum,
	StringIndex,
	StringMinimum,
	StringMaximum,
	Delimiter,
	MaxLocalItemTags,
};

enum {
	MAIN_ITEM = 0,
	GLOBAL_ITEM,
	LOCAL_ITEM,
	MAX_ITEM_TYPE,
};

struct usage_page *find_usage_page_by_id(char *id);
struct usage_page *find_usage_page_by_index(int);
struct usage *find_usage_by_id(struct usage_page *p, char *id);
struct usage *find_usage_by_index(struct usage_page *p, int);
void load_usage_db(const char *fname);
struct hid_item *parse_report_descriptor(char *str);
void print_report_descriptor(struct hid_item *);

struct hid_item *item_new(int type, int tag);

#endif
